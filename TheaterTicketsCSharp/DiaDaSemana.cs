﻿namespace TheaterTicketsCSharp
{
    public enum DiaDaSemana
    {
        Dom, Seg, Ter, Qua, Qui, Sex, Sab, Feriado
    }
}
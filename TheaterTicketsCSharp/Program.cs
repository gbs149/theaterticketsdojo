﻿using System;

namespace TheaterTicketsCSharp
{
    class Program
    {
        static void Main(string[] args)
        {

            IngressoFactory ingressoFactory = new IngressoFactory();

            IIngresso ingressoCrianca = ingressoFactory.GetIngresso(TipoDeIngresso.Crianca);

            Console.WriteLine(ingressoCrianca.CalcularPreco(DiaDaSemana.Seg));
        }
    }
}

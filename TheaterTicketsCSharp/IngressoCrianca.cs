﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheaterTicketsCSharp
{
    class IngressoCrianca : IIngresso
    {
        public IngressoCrianca(decimal preco)
        {
            Preco = preco;
        }

        public decimal Preco {get;set;}

        public decimal CalcularDesconto(DiaDaSemana dia)
        {
            decimal desconto = Preco * DescontoCrianca(dia) / 100;
            return desconto;
        }

        public decimal CalcularPreco(DiaDaSemana dia)
        {
            decimal desconto = CalcularDesconto(dia);
            decimal precoComDesconto = Preco - desconto;

            return precoComDesconto;
        }


        private int DescontoCrianca(DiaDaSemana dia)
        {
            switch (dia)
            {
                case DiaDaSemana.Seg:
                    return 10;

                case DiaDaSemana.Ter:
                    return 15;

                case DiaDaSemana.Qua:
                    return 30;

                case DiaDaSemana.Sex:
                    return 11;

                default:
                    return 0;
            }
        }
    }
}

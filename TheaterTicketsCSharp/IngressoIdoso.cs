﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheaterTicketsCSharp
{
    class IngressoIdoso : IIngresso
    {
        public IngressoIdoso(decimal preco)
        {
            Preco = preco;
        }

        public decimal Preco { get; set; }

        public decimal CalcularDesconto(DiaDaSemana dia)
        {
            decimal desconto = Preco * DescontoIdoso(dia) / 100;
            return desconto;
        }

        public decimal CalcularPreco(DiaDaSemana dia)
        {
            decimal desconto = CalcularDesconto(dia);
            decimal precoComDesconto = Preco - desconto;

            return precoComDesconto;
        }


        private int DescontoIdoso(DiaDaSemana dia)
        {
            switch (dia)
            {
                case DiaDaSemana.Seg:
                    return 10;

                case DiaDaSemana.Ter:
                    return 15;

                case DiaDaSemana.Qua:
                    return 40;

                case DiaDaSemana.Qui:
                    return 30;

                case DiaDaSemana.Sab:
                case DiaDaSemana.Dom:
                case DiaDaSemana.Feriado:
                    return 5;

                default:
                    return 0;
            }
        }
    }
}

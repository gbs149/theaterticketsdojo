﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheaterTicketsCSharp
{
    class IngressoEstudante : IIngresso
    {
        public IngressoEstudante(decimal preco)
        {
            Preco = preco;
        }

        public decimal Preco { get; set; }

        public decimal CalcularDesconto(DiaDaSemana dia)
        {
            decimal desconto = Preco * DescontoEstudante(dia) / 100;
            return desconto;
        }

        public decimal CalcularPreco(DiaDaSemana dia)
        {
            decimal desconto = CalcularDesconto(dia);
            decimal precoComDesconto = Preco - desconto;

            return precoComDesconto;
        }


        private int DescontoEstudante(DiaDaSemana dia)
        {
            switch (dia)
            {
                case DiaDaSemana.Seg:
                case DiaDaSemana.Ter:
                case DiaDaSemana.Qui:
                case DiaDaSemana.Sex:
                    return 35;

                case DiaDaSemana.Qua:
                    return 50;

                default:
                    return 0;
            }
        }
    }
}

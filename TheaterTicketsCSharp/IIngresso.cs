﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheaterTicketsCSharp
{
    public interface IIngresso
    {
        decimal Preco { get; set; }
        decimal CalcularPreco(DiaDaSemana dia);
        decimal CalcularDesconto(DiaDaSemana dia);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheaterTicketsCSharp
{
    public class IngressoFactory
    {
        private decimal precoCrianca = 5.5m;
        private decimal precoEstudante = 8;
        private decimal precoIdoso = 6;

        public IIngresso GetIngresso(TipoDeIngresso tipo)
        {
            switch(tipo)
            {
                case TipoDeIngresso.Crianca:
                    return new IngressoCrianca(precoCrianca);

                case TipoDeIngresso.Estudante:
                    return new IngressoEstudante(precoEstudante);

                case TipoDeIngresso.Idoso:
                    return new IngressoIdoso(precoIdoso);

                default:
                    return null;
            }
        }
    }
}

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TheaterTicketsCSharp;

namespace TheaterTicketsTests
{
    [TestClass]
    public class IngressoTestes
    {
        [ClassInitialize]
        public static void CreateIngresso(TestContext TC)
        {
            IngressoFactory ingressoFactory = new IngressoFactory();
            _IngressoCrianca = ingressoFactory.GetIngresso(TipoDeIngresso.Crianca);
            _IngressoEstudante = ingressoFactory.GetIngresso(TipoDeIngresso.Estudante);
            _IngressoIdoso = ingressoFactory.GetIngresso(TipoDeIngresso.Idoso);
        }

        private static IIngresso _IngressoCrianca;
        private static IIngresso _IngressoEstudante;
        private static IIngresso _IngressoIdoso;



        [TestMethod]
        public void Ingresso_Para_Crianca_Tem_10_Porcento_de_Desconto_na_Segunda()
        {
            decimal preco = _IngressoCrianca.CalcularPreco(DiaDaSemana.Seg);
            decimal expected = _IngressoCrianca.Preco - (10 * _IngressoCrianca.Preco / 100);
            Assert.AreEqual(expected, preco);         
        }

        [TestMethod]
        public void Ingresso_Para_Crianca_Tem_15_Porcento_de_Desconto_na_Terca()
        {
            decimal preco = _IngressoCrianca.CalcularPreco(DiaDaSemana.Ter);
            decimal expected = _IngressoCrianca.Preco - (15 * _IngressoCrianca.Preco / 100);
            Assert.AreEqual(expected, preco);
        }

        [TestMethod]
        public void Ingresso_Para_Crianca_Tem_30_Porcento_de_Desconto_na_Quarta()
        {
            decimal preco = _IngressoCrianca.CalcularPreco(DiaDaSemana.Qua);
            decimal expected = _IngressoCrianca.Preco - (30 * _IngressoCrianca.Preco / 100);
            Assert.AreEqual(expected, preco);
        }

        [TestMethod]
        public void Ingresso_Para_Crianca_Nao_Tem_Desconto_na_Quinta()
        {
            decimal preco = _IngressoCrianca.CalcularPreco(DiaDaSemana.Qui);
            decimal expected = _IngressoCrianca.Preco;
            Assert.AreEqual(expected, preco);
        }

        [TestMethod]
        public void Ingresso_Para_Crianca_Tem_11_Porcento_de_Desconto_na_Sexta()
        {
            decimal preco = _IngressoCrianca.CalcularPreco(DiaDaSemana.Sex);
            decimal expected = _IngressoCrianca.Preco - (11 * _IngressoCrianca.Preco / 100);
            Assert.AreEqual(expected, preco);
        }
        
        [TestMethod]
        public void Ingresso_Para_Crianca_Nao_Tem_Desconto_no_Sabado()
        {
            decimal preco = _IngressoCrianca.CalcularPreco(DiaDaSemana.Sab);
            decimal expected = _IngressoCrianca.Preco;
            Assert.AreEqual(expected, preco);
        }

        [TestMethod]
        public void Ingresso_Para_Crianca_Nao_Tem_Desconto_no_Domingo()
        {
            decimal preco = _IngressoCrianca.CalcularPreco(DiaDaSemana.Dom);
            decimal expected = _IngressoCrianca.Preco;
            Assert.AreEqual(expected, preco);
        }

        [TestMethod]
        public void Ingresso_Para_Crianca_Nao_Tem_Desconto_no_Feriado()
        {
            decimal preco = _IngressoCrianca.CalcularPreco(DiaDaSemana.Feriado);
            decimal expected = _IngressoCrianca.Preco;
            Assert.AreEqual(expected, preco);
        }



        [TestMethod]
        public void Ingresso_Para_Estudante_Tem_35_Porcento_de_Desconto_na_Segunda()
        {
            decimal preco = _IngressoEstudante.CalcularPreco(DiaDaSemana.Seg);
            decimal expected = _IngressoEstudante.Preco - (35 * _IngressoEstudante.Preco / 100);
            Assert.AreEqual(expected, preco);
        }

        [TestMethod]
        public void Ingresso_Para_Estudante_Tem_35_Porcento_de_Desconto_na_Terca()
        {
            decimal preco = _IngressoEstudante.CalcularPreco(DiaDaSemana.Ter);
            decimal expected = _IngressoEstudante.Preco - (35 * _IngressoEstudante.Preco / 100);
            Assert.AreEqual(expected, preco);
        }

        [TestMethod]
        public void Ingresso_Para_Estudante_Tem_50_Porcento_de_Desconto_na_Quarta()
        {
            decimal preco = _IngressoEstudante.CalcularPreco(DiaDaSemana.Qua);
            decimal expected = _IngressoEstudante.Preco - (50 * _IngressoEstudante.Preco / 100);
            Assert.AreEqual(expected, preco);
        }

        [TestMethod]
        public void Ingresso_Para_Estudante_Tem_35_Porcento_de_Desconto_na_Quinta()
        {
            decimal preco = _IngressoEstudante.CalcularPreco(DiaDaSemana.Qui);
            decimal expected = _IngressoEstudante.Preco - (35 * _IngressoEstudante.Preco / 100);
            Assert.AreEqual(expected, preco);
        }

        [TestMethod]
        public void Ingresso_Para_Estudante_Tem_35_Porcento_de_Desconto_na_Sexta()
        {
            decimal preco = _IngressoEstudante.CalcularPreco(DiaDaSemana.Sex);
            decimal expected = _IngressoEstudante.Preco - (35 * _IngressoEstudante.Preco / 100);
            Assert.AreEqual(expected, preco);
        }

        [TestMethod]
        public void Ingresso_Para_Estudante_Nao_Tem_Desconto_no_Sabado()
        {
            decimal preco = _IngressoEstudante.CalcularPreco(DiaDaSemana.Sab);
            decimal expected = _IngressoEstudante.Preco;
            Assert.AreEqual(expected, preco);
        }

        [TestMethod]
        public void Ingresso_Para_Estudante_Nao_Tem_Desconto_no_Domingo()
        {
            decimal preco = _IngressoEstudante.CalcularPreco(DiaDaSemana.Dom);
            decimal expected = _IngressoEstudante.Preco;
            Assert.AreEqual(expected, preco);
        }

        [TestMethod]
        public void Ingresso_Para_Estudante_Nao_Tem_Desconto_no_Feriado()
        {
            decimal preco = _IngressoEstudante.CalcularPreco(DiaDaSemana.Feriado);
            decimal expected = _IngressoEstudante.Preco;
            Assert.AreEqual(expected, preco);
        }


        
        [TestMethod]
        public void Ingresso_Para_Idoso_Tem_10_Porcento_de_Desconto_na_Segunda()
        {
            decimal preco = _IngressoIdoso.CalcularPreco(DiaDaSemana.Seg);
            decimal expected = _IngressoIdoso.Preco - (10 * _IngressoIdoso.Preco / 100);
            Assert.AreEqual(expected, preco);
        }

        [TestMethod]
        public void Ingresso_Para_Idoso_Tem_15_Porcento_de_Desconto_na_Terca()
        {
            decimal preco = _IngressoIdoso.CalcularPreco(DiaDaSemana.Ter);
            decimal expected = _IngressoIdoso.Preco - (15 * _IngressoIdoso.Preco / 100);
            Assert.AreEqual(expected, preco);
        }

        [TestMethod]
        public void Ingresso_Para_Idoso_Tem_40_Porcento_de_Desconto_na_Quarta()
        {
            decimal preco = _IngressoIdoso.CalcularPreco(DiaDaSemana.Qua);
            decimal expected = _IngressoIdoso.Preco - (40 * _IngressoIdoso.Preco / 100);
            Assert.AreEqual(expected, preco);
        }

        [TestMethod]
        public void Ingresso_Para_Idoso_Tem_30_Porcento_de_Desconto_na_Quinta()
        {
            decimal preco = _IngressoIdoso.CalcularPreco(DiaDaSemana.Qui);
            decimal expected = _IngressoIdoso.Preco - (30 * _IngressoIdoso.Preco / 100);
            Assert.AreEqual(expected, preco);
        }

        [TestMethod]
        public void Ingresso_Para_Idoso_Nao_Tem_Desconto_na_Sexta()
        {
            decimal preco = _IngressoIdoso.CalcularPreco(DiaDaSemana.Sex);
            decimal expected = _IngressoIdoso.Preco;
            Assert.AreEqual(expected, preco);
        }

        [TestMethod]
        public void Ingresso_Para_Idoso_Tem_5_Porcento_de_Desconto_no_Sabado()
        {
            decimal preco = _IngressoIdoso.CalcularPreco(DiaDaSemana.Sab);
            decimal expected = _IngressoIdoso.Preco - (5 * _IngressoIdoso.Preco / 100);
            Assert.AreEqual(expected, preco);
        }

        [TestMethod]
        public void Ingresso_Para_Idoso_Tem_5_Porcento_de_Desconto_no_Domingo()
        {
            decimal preco = _IngressoIdoso.CalcularPreco(DiaDaSemana.Dom);
            decimal expected = _IngressoIdoso.Preco - (5 * _IngressoIdoso.Preco / 100);
            Assert.AreEqual(expected, preco);
        }

        [TestMethod]
        public void Ingresso_Para_Idoso_Tem_5_Porcento_de_Desconto_no_Feriado()
        {
            decimal preco = _IngressoIdoso.CalcularPreco(DiaDaSemana.Feriado);
            decimal expected = _IngressoIdoso.Preco - (5 * _IngressoIdoso.Preco / 100);
            Assert.AreEqual(expected, preco);
        }


        


        [TestMethod]
        public void Desconto_Para_Crianca_na_Segunda_E_10_Porcento()
        {
            decimal desconto = _IngressoCrianca.CalcularDesconto(DiaDaSemana.Seg);
            decimal expected = _IngressoCrianca.Preco * 10 / 100;
            Assert.AreEqual(expected, desconto);
        }

        [TestMethod]
        public void Desconto_Para_Crianca_na_Terca_E_15_Porcento()
        {
            decimal desconto = _IngressoCrianca.CalcularDesconto(DiaDaSemana.Ter);
            decimal expected = _IngressoCrianca.Preco * 15 / 100;
            Assert.AreEqual(expected, desconto);
        }

        [TestMethod]
        public void Desconto_Para_Crianca_na_Quarta_E_30_Porcento()
        {
            decimal desconto = _IngressoCrianca.CalcularDesconto(DiaDaSemana.Qua);
            decimal expected = _IngressoCrianca.Preco * 30 / 100;
            Assert.AreEqual(expected, desconto);
        }

        [TestMethod]
        public void Crianca_Nao_Tem_Desconto_na_Quinta()
        {
            decimal desconto = _IngressoCrianca.CalcularDesconto(DiaDaSemana.Qui);
            decimal expected = 0;
            Assert.AreEqual(expected, desconto);
        }

        [TestMethod]
        public void Desconto_Para_Crianca_na_Sexta_E_11_Porcento()
        {
            decimal desconto = _IngressoCrianca.CalcularDesconto(DiaDaSemana.Sex);
            decimal expected = _IngressoCrianca.Preco * 11 / 100;
            Assert.AreEqual(expected, desconto);
        }

        [TestMethod]
        public void Crianca_Nao_Tem_Desconto_no_Sabado()
        {
            decimal desconto = _IngressoCrianca.CalcularDesconto(DiaDaSemana.Sab);
            decimal expected = 0;
            Assert.AreEqual(expected, desconto);
        }

        [TestMethod]
        public void Crianca_Nao_Tem_Desconto_no_Domingo()
        {
            decimal desconto = _IngressoCrianca.CalcularDesconto(DiaDaSemana.Dom);
            decimal expected = 0;
            Assert.AreEqual(expected, desconto);
        }

        [TestMethod]
        public void Crianca_Nao_Tem_Desconto_em_Feriados()
        {
            decimal desconto = _IngressoCrianca.CalcularDesconto(DiaDaSemana.Feriado);
            decimal expected = 0;
            Assert.AreEqual(expected, desconto);
        }



        [TestMethod]
        public void Desconto_Para_Estudante_na_Segunda_E_35_Porcento()
        {
            decimal desconto = _IngressoEstudante.CalcularDesconto(DiaDaSemana.Seg);
            decimal expected = _IngressoEstudante.Preco * 35 / 100;
            Assert.AreEqual(expected, desconto);
        }

        [TestMethod]
        public void Desconto_Para_Estudante_na_Terca_E_35_Porcento()
        {
            decimal desconto = _IngressoEstudante.CalcularDesconto(DiaDaSemana.Ter);
            decimal expected = _IngressoEstudante.Preco * 35 / 100;
            Assert.AreEqual(expected, desconto);
        }

        [TestMethod]
        public void Desconto_Para_Estudante_na_Quarta_E_50_Porcento()
        {
            decimal desconto = _IngressoEstudante.CalcularDesconto(DiaDaSemana.Qua);
            decimal expected = _IngressoEstudante.Preco * 50 / 100;
            Assert.AreEqual(expected, desconto);
        }

        [TestMethod]
        public void Desconto_Para_Estudante_na_Quinta_E_35_Porcento()
        {
            decimal desconto = _IngressoEstudante.CalcularDesconto(DiaDaSemana.Qui);
            decimal expected = _IngressoEstudante.Preco * 35 / 100;
            Assert.AreEqual(expected, desconto);
        }

        [TestMethod]
        public void Desconto_Para_Estudante_na_Sexta_E_35_Porcento()
        {
            decimal desconto = _IngressoEstudante.CalcularDesconto(DiaDaSemana.Sex);
            decimal expected = _IngressoEstudante.Preco * 35 / 100;
            Assert.AreEqual(expected, desconto);
        }

        [TestMethod]
        public void Estudante_Nao_Tem_Desconto_no_Sabado()
        {
            decimal desconto = _IngressoEstudante.CalcularDesconto(DiaDaSemana.Sab);
            decimal expected = 0;
            Assert.AreEqual(expected, desconto);
        }

        [TestMethod]
        public void Estudante_Nao_Tem_Desconto_no_Domingo()
        {
            decimal desconto = _IngressoEstudante.CalcularDesconto(DiaDaSemana.Dom);
            decimal expected = 0;
            Assert.AreEqual(expected, desconto);
        }

        [TestMethod]
        public void Estudante_Nao_Tem_Desconto_em_Feriados()
        {
            decimal desconto = _IngressoEstudante.CalcularDesconto(DiaDaSemana.Feriado);
            decimal expected = 0;
            Assert.AreEqual(expected, desconto);
        }



        [TestMethod]
        public void Desconto_Para_Idoso_na_Segunda_E_10_Porcento()
        {
            decimal desconto = _IngressoIdoso.CalcularDesconto(DiaDaSemana.Seg);
            decimal expected = _IngressoIdoso.Preco * 10 / 100;
            Assert.AreEqual(expected, desconto);
        }

        [TestMethod]
        public void Desconto_Para_Idoso_na_Terca_E_15_Porcento()
        {
            decimal desconto = _IngressoIdoso.CalcularDesconto(DiaDaSemana.Ter);
            decimal expected = _IngressoIdoso.Preco * 15 / 100;
            Assert.AreEqual(expected, desconto);
        }

        [TestMethod]
        public void Desconto_Para_Idoso_na_Quarta_E_40_Porcento()
        {
            decimal desconto = _IngressoIdoso.CalcularDesconto(DiaDaSemana.Qua);
            decimal expected = _IngressoIdoso.Preco * 40 / 100;
            Assert.AreEqual(expected, desconto);
        }

        [TestMethod]
        public void Desconto_Para_Idoso_na_Quinta_E_30_Porcento()
        {
            decimal desconto = _IngressoIdoso.CalcularDesconto(DiaDaSemana.Qui);
            decimal expected = _IngressoIdoso.Preco * 30 / 100;
            Assert.AreEqual(expected, desconto);
        }

        [TestMethod]
        public void Idoso_Nao_Tem_Desconto_Na_Sexta()
        {
            decimal desconto = _IngressoIdoso.CalcularDesconto(DiaDaSemana.Sex);
            decimal expected = 0;
            Assert.AreEqual(expected, desconto);
        }

        [TestMethod]
        public void Desconto_Para_Idoso_no_Sabado_E_5_Porcento()
        {
            decimal desconto = _IngressoIdoso.CalcularDesconto(DiaDaSemana.Sab);
            decimal expected = _IngressoIdoso.Preco * 5 / 100;
            Assert.AreEqual(expected, desconto);
        }

        [TestMethod]
        public void Desconto_Para_Idoso_no_Domingo_E_5_Porcento()
        {
            decimal desconto = _IngressoIdoso.CalcularDesconto(DiaDaSemana.Dom);
            decimal expected = _IngressoIdoso.Preco * 5 / 100;
            Assert.AreEqual(expected, desconto);
        }

        [TestMethod]
        public void Desconto_Para_Idoso_em_Feriados_E_5_Porcento()
        {
            decimal desconto = _IngressoIdoso.CalcularDesconto(DiaDaSemana.Feriado);
            decimal expected = _IngressoIdoso.Preco * 5 / 100;
            Assert.AreEqual(expected, desconto);
        }
    }
}
